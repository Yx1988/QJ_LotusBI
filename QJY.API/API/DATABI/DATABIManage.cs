﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using QJY.Common;
using QJY.Data;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace QJY.API
{
    public class DATABIManage
    {
        #region DataSet

        //获取数据集
        public void GETBIDBSETLIST(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string userName = UserInfo.User.UserName;
            string strWhere = " 1=1 ";

            int page = 0;
            int pagecount = 8;
            int.TryParse(context.Request("p") ?? "1", out page);
            int.TryParse(context.Request("pagecount") ?? "8", out pagecount);//页数
            page = page == 0 ? 1 : page;
            int total = 0;
            DataTable dt = new BI_DB_SetB().GetDataPager(" BI_DB_Set T left join BI_DB_Source S on T.SID=S.ID ", " T.*, IFNULL(S.Name,'本地数据源')  AS  SJY,  IFNULL(S.DBType,'本地数据源')  AS DBType  ", pagecount, page, " CRDate desc ", strWhere, ref total);


            msg.Result = dt;
            msg.Result1 = total;
            msg.Result2 = new BI_DB_SourceB().GetALLEntities();

        }

        //添加修改数据集
        public void ADDBIDBSET(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            var tt = JsonConvert.DeserializeObject<BI_DB_Set>(P1);
            if (tt.ID == 0)
            {
                tt.CRUser = UserInfo.User.UserName;
                tt.CRDate = DateTime.Now;
                new BI_DB_SetB().Insert(tt);
            }
            else
            {
                tt.UPDate = DateTime.Now;
                new BI_DB_SetB().Update(tt);
            }


            msg.Result = tt;

        }

        //删除数据集
        public void DELBIDBSET(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            try
            {
                int ID = int.Parse(P1);
                new BI_DB_SetB().Delete(d => d.ID == ID);
                new BI_DB_DimB().Delete(d => d.STID == ID);
            }
            catch (Exception ex)
            {
                msg.ErrorMsg = ex.Message;
            }

        }


        public void GETBIDBSET(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            try
            {
                int ID = int.Parse(P1);
                msg.Result = new BI_DB_SetB().GetEntity(d => d.ID == ID);
                msg.Result1 = new BI_DB_DimB().GetEntities(d => d.STID == ID && d.Dimension == "1");
                msg.Result2 = new BI_DB_DimB().GetEntities(d => d.STID == ID && d.Dimension == "2");

            }
            catch (Exception ex)
            {
                msg.ErrorMsg = ex.Message;
            }

        }

        public void UPBIDSET(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            try
            {
                string WD = context.Request("WD") ?? "";
                string DL = context.Request("DL") ?? "";
                var tt = JsonConvert.DeserializeObject<BI_DB_Set>(P1);
                tt.UPDate = DateTime.Now;
                new BI_DB_SetB().Update(tt);

                List<BI_DB_Dim> ListWD = JsonConvert.DeserializeObject<List<BI_DB_Dim>>(WD);
                List<BI_DB_Dim> ListDL = JsonConvert.DeserializeObject<List<BI_DB_Dim>>(DL);
                new BI_DB_DimB().Delete(D => D.STID == tt.ID);

                ListWD.ForEach(D => D.CRDate = DateTime.Now);
                ListWD.ForEach(D => D.ColumnSource = "0");
                ListWD.ForEach(D => D.CRUser = UserInfo.User.UserName);

                ListDL.ForEach(D => D.CRDate = DateTime.Now);
                ListDL.ForEach(D => D.ColumnSource = "0");
                ListDL.ForEach(D => D.CRUser = UserInfo.User.UserName);

                new BI_DB_DimB().Insert(ListWD);
                new BI_DB_DimB().Insert(ListDL);


            }
            catch (Exception ex)
            {
                msg.ErrorMsg = ex.Message;
            }

        }


        /// <summary>
        /// 仪表盘页面使用数据集数据
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETYBDATASET(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            try
            {
                DataTable dt = new BI_DB_SetB().GetDTByCommand("select *  from BI_DB_Set  ORDER BY  ID DESC");
                dt.Columns.Add("wd", Type.GetType("System.Object"));
                dt.Columns.Add("dl", Type.GetType("System.Object"));
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    dt.Rows[i]["wd"] = new BI_DB_SetB().GetDTByCommand("select * from BI_DB_Dim WHERE STID='" + dt.Rows[i]["ID"].ToString() + "' AND Dimension='1' AND ColumnSource='0' ORDER BY  ColumnName");
                    dt.Rows[i]["dl"] = new BI_DB_SetB().GetDTByCommand("select * from BI_DB_Dim WHERE STID='" + dt.Rows[i]["ID"].ToString() + "' AND Dimension='2' AND ColumnSource='0' ORDER BY  ColumnName");
                }
                msg.Result = dt;
            }
            catch (Exception ex)
            {
                msg.ErrorMsg = ex.Message;
            }

        }



        public void JXSQL(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            try
            {
                int ID = int.Parse(P1);
                BI_DB_Set DS = new BI_DB_SetB().GetEntity(d => d.ID == ID);
                DBFactory db = new BI_DB_SourceB().GetDB(DS.SID.Value);
                DataTable dt = new DataTable();
                dt = db.GetSQL(CommonHelp.Filter(P2));
                List<BI_DB_Dim> ListDIM = new BI_DB_SetB().getCType(dt);
                ListDIM.ForEach(D => D.STID = ID);
                msg.Result = ListDIM.Where(D => D.Dimension == "1");
                msg.Result1 = ListDIM.Where(D => D.Dimension == "2");

            }
            catch (Exception ex)
            {
                msg.ErrorMsg = ex.Message;
            }

        }




        #endregion

        #region DataSource
        //测试数据源连接
        public void TESTBIDBSOURCE(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            var tt = JsonConvert.DeserializeObject<BI_DB_Source>(P1);
            var db = new DBFactory(tt.DBType, tt.DBIP, tt.Port, tt.DBName, tt.DBUser, tt.DBPwd);
            if (db.TestConn())
            {
                msg.Result = "1"; //1：代表连接成功
            }
            else
            {
                msg.ErrorMsg = "连接失败";
            }

        }

        //获取数据源
        public void GETBIDBSOURCELIST(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string userName = UserInfo.User.UserName;
            string strWhere = "1=1";
            int page = 0;
            int pagecount = 8;
            int.TryParse(context.Request("p") ?? "1", out page);
            int.TryParse(context.Request("pagecount") ?? "8", out pagecount);//页数
            page = page == 0 ? 1 : page;
            int total = 0;
            List<BI_DB_Source> dt = new BI_DB_SourceB().Db.Queryable<BI_DB_Source>().Where(strWhere).OrderBy(it => it.CRDate, OrderByType.Desc).ToPageList(page, pagecount, ref total);


            foreach (var tt in dt)
            {
                var db = new DBFactory(tt.DBType, tt.DBIP, tt.Port, tt.DBName, tt.DBUser, tt.DBPwd);
                tt.Attach = "1";//不可用
            }

            msg.Result = dt;
            msg.Result1 = total;
        }

        //添加修改数据源
        public void ADDBIDBSOURCE(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            var tt = JsonConvert.DeserializeObject<BI_DB_Source>(P1);
            if (tt.ID == 0)
            {
                tt.CRUser = UserInfo.User.UserName;
                tt.CRDate = DateTime.Now;
                new BI_DB_SourceB().Insert(tt);
            }
            else
            {
                new BI_DB_SourceB().Update(tt);
            }
            msg.Result = tt;

        }

        //删除数据源
        public void DELBIDBSOURCE(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            try
            {
                int ID = int.Parse(P1);
                new BI_DB_SourceB().Delete(d => d.ID == ID);
                new BI_DB_SetB().Delete(d => d.SID == ID);

            }
            catch (Exception ex)
            {
                msg.ErrorMsg = ex.Message;
            }

        }


        //获取数据集表名和视图名
        public void GETBIDBSOURCEVIEWLIST(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int ID = Int32.Parse(P1);
            DBFactory db = new BI_DB_SourceB().GetDB(ID);
            msg.Result = db.GetDBTables();

        }


        /// <summary>
        /// 生成数据集
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>

        public void ADDBISETLIST(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int ID = Int32.Parse(P1);
            DBFactory db = new BI_DB_SourceB().GetDB(ID);
            string strTableName = P2;
            string strDataSetName = context.Request("DsetName") ?? "1";





            BI_DB_Set DS = new BI_DB_Set();
            DS.Name = strDataSetName;
            DS.SID = ID;
            DS.SName = strTableName;
            DS.CRDate = DateTime.Now;
            DS.CRUser = UserInfo.User.UserName;
            DS.Type = "SQL";
            DS.DSQL = "SELECT  * FROM " + strTableName;
            new BI_DB_SetB().Insert(DS);




            DataTable dt = db.GetDBClient().SqlQueryable<Object>(CommonHelp.Filter("SELECT  * FROM " + strTableName)).ToDataTablePage(1, 1);

            List<BI_DB_Dim> ListDIM = new BI_DB_SetB().getCType(dt);
            ListDIM.ForEach(D => D.STID = DS.ID);
            ListDIM.ForEach(D => D.CRDate = DateTime.Now);
            ListDIM.ForEach(D => D.CRUser = UserInfo.User.UserName);

            new BI_DB_DimB().Insert(ListDIM);




        }
        #endregion



        #region YBP
        public void GETYBLISTDATA(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            msg.Result = new BI_DB_YBPB().GetALLEntities();

        }


        public void SAVEDATA(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            BI_DB_YBP model = new BI_DB_YBP();
            model.Name = P1;
            model.YBType = P2;
            model.CRUser = UserInfo.User.UserName;
            model.CRDate = DateTime.Now;
            model.Remark = new CommonHelp().GenerateCheckCode(12);
            new BI_DB_YBPB().Insert(model);
            msg.Result = model;
        }

        public void UPYBDATA(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {

            string strFormName = context.Request("FormName") ?? "";
            string strFB = context.Request("ISFB") ?? "N";



            int ID = Int32.Parse(P1);
            BI_DB_YBP model = new BI_DB_YBPB().GetEntities(d => d.ID == ID).FirstOrDefault();
            model.YBContent = P2;
            if (strFormName != "")
            {
                model.Name = strFormName;
            }
            if (strFB == "Y")
            {
                model.YBOption = P2;
            }
            if (string.IsNullOrEmpty(model.Remark))
            {
                model.Remark = new CommonHelp().GenerateCheckCode(12);
            }
            new BI_DB_YBPB().Update(model);
            msg.Result = model;
        }



        public void DELYBDATA(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int ID = Int32.Parse(P1);
            new BI_DB_YBPB().Delete(D => D.ID == ID);
            new BI_DB_DimB().Delete(D => D.STID == ID);
        }


        public void GETYBBYID(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int ID = Int32.Parse(P1);
            BI_DB_YBP model = new BI_DB_YBPB().GetEntities(d => d.ID == ID).FirstOrDefault();
            msg.Result = model;
        }


        /// <summary>
        /// 获取仪表盘数据接口
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETYBDATA(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            try
            {
                msg.DataLength = 0;
                JObject wigdata = JObject.Parse(P1);



                string datatype = (string)wigdata["datatype"];//数据来源类型0:SQL,1:API
                if (datatype == "0")//SQL取数据
                {
                    string strWigdetType = (string)wigdata["wigdetype"];
                    string strDateSetName = (string)wigdata["datasetname"];
                    JObject orderdata = (JObject)wigdata["dataorder"];

                    string ordersql = "";
                    string strPageCount = context.Request("pagecount") ?? "10";
                    string strquerydata = context.Request("querydata") ?? "";//查询条件数据
                    string strWhere = " 1=1 ";
                    BI_DB_Set DS = new BI_DB_SetB().GetEntities(d => d.Name == strDateSetName).FirstOrDefault();
                    DBFactory db = new BI_DB_SourceB().GetDB(DS.SID.Value);




                    if (strWigdetType == "dwig")
                    {
                        JArray wdlist = (JArray)wigdata["wdlist"];
                        JArray dllist = (JArray)wigdata["dllist"];
                        JArray filist = (JArray)wigdata["filist"];

                        string strWD = "";
                        string strWDGroup = "";//处理MYSQL GROUP别名问题
                        foreach (JObject item in filist)
                        {
                            string strWDType = (string)item["coltype"];
                            string strWDColumCode = (string)item["colid"];

                            //获取维度字段筛选条件
                            JArray querylist = (JArray)item["querydata"];
                            foreach (JObject queryitem in querylist)
                            {
                                string strcal = (string)queryitem["cal"];
                                string strglval = (string)queryitem["glval"];
                                switch (strcal)
                                {
                                    case "0": strWhere += " and " + strWDColumCode + " ='" + strglval + "'"; break;
                                    case "1": strWhere += " and " + strWDColumCode + " <'" + strglval + "'"; break;
                                    case "2": strWhere += " and " + strWDColumCode + " >'" + strglval + "'"; break;
                                    case "3": strWhere += " and " + strWDColumCode + " !='" + strglval + "'"; break;
                                    case "4": strWhere += " and " + strWDColumCode + "  IN  ('" + strglval.ToFormatLike() + "')"; break;
                                }

                            }
                            string bindwig = (string)item["bindwig"];

                            //处理组件筛选
                            getqwig(strquerydata, strWDColumCode, bindwig, ref strWhere);

                        }
                        foreach (JObject item in wdlist)
                        {
                            string strWDType = (string)item["coltype"];
                            string strWDColumCode = (string)item["colid"];


                            string strTempGroup = strWDColumCode;

                            if (strWDType == "TA")//分析字段
                            {
                                string strFiled = strWDColumCode.Split('_')[0];
                                string strForMat = strWDColumCode.Split('_')[1];

                                strWDColumCode = SqlHelp.TADate(strWDColumCode, db.GetDBType().ToUpper(), ref strTempGroup);
                            }

                            strWD = strWD + strWDColumCode + ",";
                            strWDGroup = strWDGroup + strTempGroup + ",";


                            //获取维度字段筛选条件
                            JArray querylist = (JArray)item["querydata"];
                            foreach (JObject queryitem in querylist)
                            {
                                string strcal = (string)queryitem["cal"];
                                string strglval = (string)queryitem["glval"];
                                switch (strcal)
                                {
                                    case "0": strWhere += " and " + strTempGroup + " ='" + strglval + "'"; break;
                                    case "1": strWhere += " and " + strTempGroup + " <'" + strglval + "'"; break;
                                    case "2": strWhere += " and " + strTempGroup + " >'" + strglval + "'"; break;
                                    case "3": strWhere += " and " + strTempGroup + " !='" + strglval + "'"; break;
                                    case "4": strWhere += " and " + strTempGroup + "  IN  ('" + strglval.ToFormatLike() + "')"; break;
                                }

                            }
                            string bindwig = (string)item["bindwig"];

                            //处理组件筛选
                            getqwig(strquerydata, strWDColumCode, bindwig, ref strWhere);


                            //处理排序
                            if ((string)orderdata["prop"].ToString() == strWDColumCode)
                            {
                                ordersql = strWDColumCode + " " + (string)orderdata["order"].ToString();
                            }

                        }
                        strWD = strWD.TrimEnd(',');
                        strWDGroup = strWDGroup.TrimEnd(',');

                        string strDL = "";
                        string strHaving = "HAVING";
                        foreach (JObject item in dllist)
                        {
                            strDL = strDL + " " + (string)item["caltype"] + "(" + (string)item["colid"] + ") AS " + (string)item["colid"] + ",";
                            string strTJFiled = (string)item["caltype"] + "(" + (string)item["colid"] + ")  ";
                            //获取统计字段筛选条件
                            JArray querylist = (JArray)item["querydata"];
                            foreach (JObject queryitem in querylist)
                            {
                                string strcal = (string)queryitem["cal"];
                                string strglval = (string)queryitem["glval"];

                                string strPre = strHaving == "HAVING" ? " " : " and ";
                                switch (strcal)
                                {
                                    case "0":
                                        strHaving += strPre + strTJFiled + " ='" + strglval + "'";
                                        break;
                                    case "1":
                                        strHaving += strPre + strTJFiled + " <'" + strglval + "'";
                                        break;
                                    case "2":
                                        strHaving += strPre + strTJFiled + " >'" + strglval + "'";
                                        break;
                                    case "3":
                                        strHaving += strPre + strTJFiled + " !='" + strglval + "'";
                                        break;
                                    case "4":
                                        strHaving += strPre + strTJFiled + "  IN  ('" + strglval.ToFormatLike() + "')";
                                        break;
                                }

                            }
                            //处理组件筛选
                            string bindwig = (string)item["bindwig"];
                            getqwig(strquerydata, strTJFiled, bindwig, ref strHaving);

                            //处理排序
                            if ((string)orderdata["prop"].ToString() == (string)item["colid"])
                            {
                                ordersql = strTJFiled + " " + (string)orderdata["order"].ToString();
                            }

                        }
                        strDL = strDL.TrimEnd(',');
                        strHaving = strHaving == "HAVING" ? "" : strHaving;
                        strHaving = strHaving.Replace("HAVING", "");

                        int pageNo = int.Parse(context.Request("pageNo") ?? "1");
                        int pageSize = int.Parse(context.Request("pageSize") ?? "0");
                        int recordTotal = 0;
                        string strRSQL = "";
                        DataTable dt = db.GetYBData(DS, strWD, strDL, strPageCount, strWhere, ordersql, pageNo, pageSize, strWDGroup, strHaving, ref recordTotal, ref strRSQL);
                        if (dt.Rows.Count > 8000)
                        {
                            msg.ErrorMsg = "返回数据量太大,超过8000,服务器罢工";
                            dt = new DataTable();
                        }
                        else
                        {
                            msg.Result = dt;
                        }
                        msg.DataLength = recordTotal;
                        msg.Result1 = strRSQL;
                    }
                }
                else if (datatype == "3")//存储过程
                {
                    //string strAPIUrl = (string)wigdata["apiurl"] + "&szhlcode=" + UserInfo.User.pccode;
                    //string str = CommonHelp.GetAPIData(strAPIUrl);
                    List<SugarParameter> ListP = new List<SugarParameter>();
                    string strProname = (string)wigdata["proname"];
                    JArray prlist = (JArray)wigdata["proqdata"];
                    foreach (var item in prlist)
                    {
                        string pname = (string)item["pname"];
                        string pvalue = (string)item["pvalue"];
                        ListP.Add(new SugarParameter(pname, pvalue));
                    }
                    // DataTable dt = new BI_DB_SetB().Db.Ado.UseStoredProcedure().GetDataTable("pr_yhcd_yhdm_kczx_new", new { _whfs = "查询教师学生常用功能", _yhdm = "000065", _parent_dm = "51", _xh = "12" });
                    DataTable dt = new BI_DB_SetB().Db.Ado.UseStoredProcedure().GetDataTable(strProname, ListP);
                    msg.Result = dt;
                }
                else
                {
                    string strAPIUrl = (string)wigdata["apiurl"];
                    string str = CommonHelp.HttpGet(strAPIUrl);
                    msg.Result = str;
                }

            }
            catch (Exception ex)
            {
                msg.ErrorMsg = "获取数据错误";
            }

        }

        /// <summary>
        /// 根据关联查询组件获取查询条件语句
        /// </summary>
        /// <param name="strquerydata"></param>
        /// <param name="strcolid"></param>
        /// <param name="strwigcode"></param>
        public void getqwig(string strquerydata, string strcolid, string strwigcode, ref string strWhere)
        {
            JArray categories = JArray.Parse(strquerydata);
            foreach (JObject item in categories)
            {

                string FiledName = strcolid;
                if (strwigcode == (string)item["wigdetcode"])
                {
                    string ColumnType = (string)item["ColumnType"];
                    string eltype = (string)item["component"];
                    if (eltype == "qjInput")
                    {
                        string strValue = (string)item["value"];
                        if (!string.IsNullOrEmpty(strValue))
                        {
                            string strSQL = string.Format(" AND {0} LIKE ('%{1}%')", FiledName.Replace(',', '+'), strValue);
                            strWhere = strWhere + strSQL;
                        }
                    }
                    else
                    if (eltype == "qjSelect")
                    {
                        string strValue = (string)item["value"];
                        if (!string.IsNullOrEmpty(strValue))
                        {
                            string strSQL = string.Format(" AND {0} IN ('{1}')", FiledName, strValue);
                            strWhere = strWhere + strSQL;
                        }
                    }
                    else
                    if (eltype == "qjMonth" || eltype == "qjDate")
                    {
                        if (item["value"] != null && item["value"].ToString() != "")
                        {
                            string strval = item["value"].ToString();
                            string sDate = strval.Split(',')[0].ToString();
                            string eDate = strval.Split(',')[1].ToString();
                            string strSQL = string.Format(" AND {0} BETWEEN '{1} 00:00' AND '{2} 23:59' ", FiledName, sDate, eDate);
                            strWhere = strWhere + strSQL;
                        }

                    }
                    else
                    {
                        string strValue = (string)item["value"];
                        if (!string.IsNullOrEmpty(strValue))
                        {
                            string strSQL = string.Format(" AND {0} IN ('{1}')", FiledName, strValue);
                            strWhere = strWhere + strSQL;
                        }
                    }
                }


            }

        }



        /// <summary>
        /// 验证API数据接口
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void YZAPIDATA(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            try
            {
                string strAPIUrl = P1 + "&szhlcode=" + UserInfo.User.pccode;
                msg.Result = CommonHelp.GetAPIData(strAPIUrl);
            }
            catch (Exception ex)
            {
                msg.ErrorMsg = ex.Message;
            }

        }

        public void GETSQLDATA(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string SQL = CommonHelp.Filter(P1);
            DBFactory db = new BI_DB_SourceB().GetDB(0);
            //var dt = new Dictionary<string, object>();
            //dt.Add("ID", "6988");
            //dt.Add("Remark1", "123");
            //dt.Add("Remark2", "asdasd");
            //db.UpdateData(dt, "JH_Auth_ZiDian");
            DataTable dt = db.GetSQL(SQL);
            msg.Result = dt;
        }

        #endregion

    }
}